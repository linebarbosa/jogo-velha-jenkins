#!/bin/bash

scp -o StrictHostKeyChecking=no provision.pp root@159.65.245.115:
scp -o StrictHostKeyChecking=no killJava.sh root@159.65.245.115:/usr/bin/
scp -o StrictHostKeyChecking=no target/jogovelha-0.0.1-SNAPSHOT.jar root@159.65.245.115:
ssh -o StrictHostKeyChecking=no root@159.65.245.115 'dnf install -y puppet'
ssh -o StrictHostKeyChecking=no root@159.65.245.115 'chmod +x /usr/bin/killJava.sh'
ssh -o StrictHostKeyChecking=no root@159.65.245.115 'puppet apply provision.pp'


#!/bin/bash

PID=$(ps -eo pid,comm | awk '$2 == "java" {print $1 }' | sed -n '1p' )

if [ -z "$PID" ]; then
echo "... not running"
else
echo "... running"
kill -9 "$PID"
fi

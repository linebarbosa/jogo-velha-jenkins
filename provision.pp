package { 'java-1.8.0-openjdk':
	ensure => latest,
}

exec { 'kill java processes':
	path => '/usr/bin',
	command => '/usr/bin/killJava.sh',
}

exec { 'run jogo velha':
	command => '/usr/bin/java -jar jogovelha-0.0.1-SNAPSHOT.jar &',
	require => [ Exec['kill java processes'], Package['java-1.8.0-openjdk'] ],
}

